#include "main.h"
#include <linux/ioctl.h> 
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <cstdio>
#include <string>
#include <cstring>

const char *usage = "Usage:\n %s get | rw | norw | clear | set <message>\n";

int main(int argc, char* argv[])
{
    size_t message_length;
    char msg_buffer[BUFFER_LEN] = "";
    if (argc < 2) {
        printf(usage, argv[0]);
        return -1;
    }
    std::string command = argv[1];
    if (command == "set" && argc < 3) {
        printf(usage, argv[0], argv[0], argv[0]);
        return -1;
    }
    int fd = open("/dev/murp", 0);
    if (fd < 0) {
        printf("Could not open file\n");
        return fd;
    }
    if (command == "set") {
        std::string message = argv[2];
        if (strlen(argv[2]) > BUFFER_LEN) {
            printf(
                "Length of given string (%lu) is more than buffer length (%d), only part will be written\n",
                strlen(argv[2]),
                BUFFER_LEN
            );
        }
        ioctl(fd, IOCTL_SET_MSG, argv[2]);
    } else if (command == "get") {
        ioctl(fd, IOCTL_GET_MSG, msg_buffer);
        printf("%s\n", msg_buffer);
    } else if (command == "clear") {
        ioctl(fd, IOCTL_DEL_MSG, 0);
    } else if (command == "rw") {
        ioctl(fd, IOCTL_CTRL_RDWR, 1);
    } else if (command == "norw") {
        ioctl(fd, IOCTL_CTRL_RDWR, 0);
    } else {
        printf(usage, argv[0]);
    }
    close(fd);
    return 0;
}