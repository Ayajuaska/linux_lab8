#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h> 
#include <linux/uaccess.h>
#include "main.h"

static int dev_opened = 0;

static char msg[BUFFER_LEN] = "Not set\n";
static char *msg_ptr;

static char *usage = "Use ioctl to communicate with module.\n";
static char *usage_ptr;
static unsigned long rdwr_enabled = 0;

static int device_open(struct inode *node, struct file *file)
{
#ifdef DEBUG
    printk(KERN_INFO "device_open(%s)\n", file);
#endif
    if (dev_opened) {
        return -EBUSY;
    } 
    dev_opened++;
    msg_ptr = msg;
    usage_ptr = usage;
    try_module_get(THIS_MODULE);
    return SUCCESS;
}

static int device_release(struct inode *node, struct file *file)
{
    dev_opened--;
    module_put(THIS_MODULE);
    return SUCCESS;
}


static ssize_t device_read_real(struct file *file,
            char __user * buffer,
            size_t length,
            loff_t * offset)
{
    ssize_t read = 0;
    while(length && *msg_ptr) {
        put_user(*(msg_ptr++), buffer++);
        length--;
        read++;
    }
    return read;
}

static ssize_t device_read(struct file *file, /* см. include/linux/fs.h   */
            char __user * buffer,             /* буфер для сообщения */
            size_t length,                    /* размер буфера       */
            loff_t * offset)
{
    ssize_t read = 0;
    if (rdwr_enabled) {
        return device_read_real(file, buffer, length, offset);
    }
    printk(KERN_INFO "buffer len %lu", length);
    while(length && *usage_ptr) {
        put_user(*(usage_ptr++), buffer++);
        length--;
        read++;
    }
    return read;
}


static ssize_t device_write_real(struct file *fl, const char __user *usr, size_t length, loff_t *offt)
{
    ssize_t written = 0;
    int i;
    i = strncpy_from_user(msg, usr, BUFFER_LEN);
    for (i = written; written < BUFFER_LEN; i++) {
        msg[i] = 0;
    }
    return written;
}


static ssize_t device_write(struct file *fl, const char __user *usr, size_t length, loff_t *offt) {
    if (rdwr_enabled) {
        return device_write_real(fl, usr, length, offt);
    }
    return length;
}

long device_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
    int i;
    switch (ioctl_num) {
        case IOCTL_GET_MSG:
            i = device_read_real(file, (char *)ioctl_param, BUFFER_LEN-1, 0);
            put_user('\0', (char *)ioctl_param + i);

            break;

        case IOCTL_SET_MSG:
            i = strncpy_from_user(msg, (char *)ioctl_param, BUFFER_LEN);
            break;

        case IOCTL_DEL_MSG:
            *msg = 0;
            break;

        case IOCTL_CTRL_RDWR:
            printk(KERN_INFO "setting rdwr_enabled = %lu\n", ioctl_param);
            rdwr_enabled = ioctl_param;
            break;
    }
    return SUCCESS;
}

struct file_operations fops = {
  .read = device_read,
  .write = device_write,
  .unlocked_ioctl = device_ioctl,
  .open = device_open,
  .release = device_release,    /* оно же close */
};

/*
 * This will be called on module load
 */
int init_module()
{
    int ret_val;
    ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &fops);
    if (ret_val < 0) {
        printk(KERN_ERR "device not registered \n");
        return ret_val;
    }
    msg_ptr = msg;
    usage_ptr = usage;
    return SUCCESS;
}

/*
 * This will be called on module unload
 */
void cleanup_module()
{
    unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
	return;
}


MODULE_AUTHOR("Denis Bukanov");
MODULE_DESCRIPTION("Drastically useless chardev driver module.");
MODULE_LICENSE("CC");