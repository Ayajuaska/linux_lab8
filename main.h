#ifndef MODULE_MAIN_H
#define MODULE_MAIN_H


#include <linux/ioctl.h>



/*
 * Module's intermal buffer size
 */
#define BUFFER_LEN 80

/*
 * SUCCESS code
 */
#define SUCCESS 0

/*
* The major device number. We can't rely on dynamic
* registration any more, because ioctls need to know
* it.
*/
#define MAJOR_NUM 282
/*
* Set the message of the device driver
*/
#define IOCTL_SET_MSG _IOR(MAJOR_NUM, 0, char *)
/*
* _IOR means that we're creating an ioctl command
* number for passing information from a user process
* to the kernel module.
*
* The first arguments, MAJOR_NUM, is the major device
* number we're using.
*
* The second argument is the number of the command
* (there could be several with different meanings).
*
* The third argument is the type we want to get from
* the process to the kernel.
*/
/*
* Get the message of the device driver
*/
#define IOCTL_GET_MSG _IOR(MAJOR_NUM, 1, char *)
/*
* This IOCTL is used for output, to get the message
* of the device driver. However, we still need the
* buffer to place the message in to be input,
* as it is allocated by the process.
*/
/*
* Get the n'th byte of the message
*/
#define IOCTL_GET_NTH_BYTE _IOWR(MAJOR_NUM, 2, int)
/*
* The IOCTL is used for both input and output. It
* receives from the user a number, n, and returns
* Message[n].
*/

/*
 * Clear message
 */
#define IOCTL_DEL_MSG _IOWR(MAJOR_NUM, 3, int)
/*
 * The IOCTL is used for input. It clear message.
 */

/*
 * Normal read-write control
 */
#define IOCTL_CTRL_RDWR _IOWR(MAJOR_NUM, 4, int)
/*
 * The IOCTL is used for enabling\disabling normal read-write.
 */

/*
* The name of the device file
*/
#define DEVICE_FILE_NAME "murp"
#define DEVICE_NAME "murp"

#endif